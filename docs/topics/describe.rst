=============
Idem describe
=============

``idem describe`` command is used to list resources in a cloud account.
``idem describe`` supports state file path and regular expression as input. Any valid regular expression can
be used as an input.

State file path as input
------------------------
This command will output all the AWS S3 buckets in the provided cloud account.

.. code-block::

    idem describe aws.s3.bucket

Regular expression as input
---------------------------
This command will output all the aws resources in the provided cloud account.

.. code-block::

    idem describe "aws.*"

This command will output all the dynamodb and s3 resources in the provided cloud account.

.. code-block::

    idem describe "aws\.(dynamodb|s3)\..*"
