===========
Idem 13.0.0
===========

Idem 13.0.0 introduces the `describe` subcommand to create SLS files.


Describe Subcommand
===================

A new subcommand, "idem describe" will call the "describe" command for the resource associated with the current account.
Using the test states built into idem, we can explore this feature.
First we will describe the "test" submodule of idem and output the yaml results to a file called "test.sls".

.. code-block:: bash

    $ idem describe test --output=yaml > test.sls

The contents of test.sls will be as follows:

.. code-block:: yaml

    Description of test.anop:
      test.anop:
      - name: anop
    Description of test.configurable_test_state:
      test.configurable_test_state:
      - name: configurable_test_state
      - changes: true
      - result: true
      - comment: ''
    Description of test.fail_with_changes:
      test.fail_with_changes:
      - name: fail_with_changes
    Description of test.fail_without_changes:
      test.fail_without_changes:
      - name: fail_without_changes
    Description of test.mod_watch:
      test.mod_watch:
      - name: mod_watch
    Description of test.none_without_changes:
      test.none_without_changes:
      - name: none_without_changes
    Description of test.nop:
      test.nop:
      - name: nop
    Description of test.succeed_with_changes:
      test.succeed_with_changes:
      - name: succeed_with_changes
    Description of test.succeed_with_comment:
      test.succeed_with_comment:
      - name: succeed_with_comment
      - comment: null
    Description of test.succeed_without_changes:
      test.succeed_without_changes:
      - name: succeed_without_changes
    Description of test.treq:
      test.treq:
      - name: treq
    Description of test.update_low:
      test.update_low:
      - name: update_low


The output of "describe" represents a completely valid idem sls file.
It shows the current status of your idem resource.
You can manage your idem resources by modifying this file then running "idem state" on the file.

.. code-block:: bash

    $ idem state test.sls

Output

.. code-block::

    --------
          ID: Description of test.anop
    Function: test.anop
      Result: True
     Comment: Success!
     Changes:
    --------
          ID: Description of test.configurable_test_state
    Function: test.configurable_test_state
      Result: True
     Comment:
     Changes: testing:
        ----------
        old:
            Unchanged
        new:
            Something pretended to change
    --------
          ID: Description of test.fail_with_changes
    Function: test.fail_with_changes
      Result: False
     Comment: Failure!
     Changes: testing:
        ----------
        old:
            Unchanged
        new:
            Something pretended to change
    --------
          ID: Description of test.fail_without_changes
    Function: test.fail_without_changes
      Result: False
     Comment: Failure!
     Changes:
    --------
          ID: Description of test.mod_watch
    Function: test.mod_watch
      Result: True
     Comment: Watch ran!
     Changes: watch:
        True
    --------
          ID: Description of test.none_without_changes
    Function: test.none_without_changes
      Result: None
     Comment: Success!
     Changes:
    --------
          ID: Description of test.nop
    Function: test.nop
      Result: True
     Comment: Success!
     Changes:
    --------
          ID: Description of test.succeed_with_changes
    Function: test.succeed_with_changes
      Result: True
     Comment: Success!
     Changes: testing:
        ----------
        old:
            Unchanged
        new:
            Something pretended to change
    --------
          ID: Description of test.succeed_with_comment
    Function: test.succeed_with_comment
      Result: True
     Comment: None
     Changes:
    --------
          ID: Description of test.succeed_without_changes
    Function: test.succeed_without_changes
      Result: True
     Comment: Success!
     Changes:
    --------
          ID: Description of test.update_low
    Function: test.update_low
      Result: True
     Comment: Success!
     Changes:
    --------
          ID: king_arthur
    Function: test.nop
      Result: True
     Comment: Success!
     Changes:
    --------
          ID: Description of test.treq
    Function: test.treq
      Result: True
     Comment: Success!
     Changes:

Implementing describe functionality
===================================

Create a function called "describe" in a `states` plugin.
It should output valid input for the `present` function of that plugin as a dictionary

.. code-block:: python

    async def describe(hub, ctx):
        # This function returns a description of the resource -- one that "idem state" can use to
        # to completely recreate the resource with different credentials
        # or manage the resource with the same credentials
        return {
            "Unique State Name": {
                "reference.to.present.function": {
                    [{"name": "resource name"}, {"resource_kwarg": "resource_value"}]
                }
            }
        }
