import fnmatch
import os
import pathlib
import subprocess
import sys
import tempfile

import yaml


def test_cli(runpy, tmpdir):
    cache_dir = tmpdir
    run_name = "test_restore"
    # Make sure it goes to the cache_file from the "local" plugin, the other cache_file is rightfully gone
    cache_file = tmpdir / "esm" / "local" / f"{run_name}.msgpack"

    try:
        with tempfile.NamedTemporaryFile(suffix=".msgpack", delete=True) as fh:
            fh.write(b'{"1":"2"}')
            fh.flush()

            with tempfile.NamedTemporaryFile(
                suffix=".cfg", delete=True, mode="w+"
            ) as cfg_fh:
                yaml.safe_dump({"idem": {"esm_keep_cache": True}}, stream=cfg_fh)
                cfg_fh.flush()

                # Run the restore command
                cmd = [
                    sys.executable,
                    runpy,
                    "restore",
                    fh.name,
                    f"--cache-dir={cache_dir}",
                    f"--run-name={run_name}",
                    f"--config={cfg_fh.name}",
                ]

                ret = subprocess.run(cmd, capture_output=True)

        assert not ret.returncode, ret.stderr

        # The data should have been transferred to the cache_file
        with cache_file.open("rb") as fh:
            contents = fh.read()
            assert contents == b"\x81\xa11\xa12"
    finally:
        cache_file.remove()


def _find_file(dir_path: pathlib.Path, pattern: str) -> pathlib.Path:
    for root, dirs, files in os.walk(top=dir_path):
        for name in files:
            if fnmatch.fnmatch(name, pattern):
                return dir_path / name
