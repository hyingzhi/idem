import pop.hub
from dict_tools.data import NamespaceDict
from pytest_idem.runner import run_yaml_block


def test_basic():
    STATE = """
    state:
      test.nop
    """
    # Initialize real hub
    hub = pop.hub.Hub()
    hub.pop.sub.add(dyne_name="idem")
    hub.OPT = NamespaceDict(idem=dict(progress=True, progress_plugin="tqdm"))

    # Initialize mock hub
    mock_hub = hub.pop.testing.mock_hub()
    hub.tool.progress.tqdm.create = mock_hub.tool.progress.tqdm.create
    hub.tool.progress.tqdm.update = mock_hub.tool.progress.tqdm.update

    # Run the states from the yaml block
    ret = run_yaml_block(STATE, hub=hub)

    # Verify that all states are included in the result
    assert len(ret) == 1

    mock_hub.tool.progress.tqdm.create.assert_called_once()
    mock_hub.tool.progress.tqdm.update.assert_called_once()


def test_disabled():
    STATE = """
    state:
      test.nop
    """
    # Initialize real hub
    hub = pop.hub.Hub()
    hub.pop.sub.add(dyne_name="idem")
    hub.OPT = NamespaceDict(idem=dict(progress=False, progress_plugin="irrelevant"))

    # Initialize mock hub
    mock_hub = hub.pop.testing.mock_hub()
    hub.tool.progress.init.create = mock_hub.tool.progress.init.create
    hub.tool.progress.init.update = mock_hub.tool.progress.init.update

    # Run the states from the yaml block
    ret = run_yaml_block(STATE, hub=hub)

    # Verify that all states are included in the result
    assert len(ret) == 1

    mock_hub.tool.progress.tqdm.create.assert_not_called()
    mock_hub.tool.progress.tqdm.update.assert_not_called()


def test_config():
    STATE = """
    state:
      test.nop
    """
    # Initialize real hub
    hub = pop.hub.Hub()
    hub.pop.sub.add(dyne_name="idem")
    hub.OPT = NamespaceDict(
        idem=dict(
            progress=True, progress_plugin="tqdm", progress_options={"asdf": "jkl;"}
        )
    )

    # Initialize mock hub
    mock_hub = hub.pop.testing.mock_hub()
    hub.tool.progress.tqdm.create = mock_hub.tool.progress.tqdm.create
    hub.tool.progress.tqdm.update = mock_hub.tool.progress.tqdm.update

    # Run the states from the yaml block
    ret = run_yaml_block(STATE, hub=hub)

    # Verify that all states are included in the result
    assert len(ret) == 1

    mock_hub.tool.progress.tqdm.create.assert_called_once()
    mock_hub.tool.progress.tqdm.update.assert_called_once()
    assert mock_hub.tool.progress.tqdm.create.call_args[1] == {
        "asdf": "jkl;",
        "desc": "idem runtime: 0",
        "unit": "states",
    }
