def test_static(hub):
    val = hub.reconcile.wait.static.get(wait_in_seconds=11)

    assert val == 11


def test_static_no_args(hub):
    try:
        hub.reconcile.wait.static.get(wait_in_seconds=None)
    except ValueError:
        assert True


def test_random_no_args(hub):
    try:
        hub.reconcile.wait.random.get(min=None, max=None)
    except ValueError:
        assert True


def test_random_invalid_args(hub):
    try:
        hub.reconcile.wait.random.get(min_value=10, max_value=2)
    except ValueError:
        assert True


def test_random(hub):
    val = hub.reconcile.wait.random.get(min_value=1, max_value=10)

    assert val <= 10
    assert val >= 1


def test_exp_invalid_arg(hub):
    try:
        hub.reconcile.wait.exponential.get(wait_in_seconds=0, multiplier=0)
    except ValueError:
        assert True


def test_exp_loop_none(hub):
    val = hub.reconcile.wait.exponential.get(wait_in_seconds=2, multiplier=3)
    assert val == 2


def test_exp_loop_zero(hub):
    val = hub.reconcile.wait.exponential.get(
        wait_in_seconds=2, multiplier=3, run_count=0
    )
    # 2 * 3^0
    assert val == 2


def test_exp_loop_one(hub):
    val = hub.reconcile.wait.exponential.get(
        wait_in_seconds=2, multiplier=3, run_count=1
    )
    # 2 * 3^1
    assert val == 6


def test_exp(hub):
    val = hub.reconcile.wait.exponential.get(
        wait_in_seconds=2, multiplier=3, run_count=4
    )
    # 2 * 3^4 = 162
    assert val == 162
