from typing import Any
from typing import Dict


async def sig_modify(hub, name: str, chunk: Dict[str, Any]) -> Dict[str, Any]:
    ...
